`timescale 1ns/100ps //se incluyen archivos y define el timescale
`include "probador.v"
`include "top.v"
// `include "cmos_cells.v"
`include "osu18_stdcells.v"
`include "top_synth.v"

module bancopruebas;
  // wire [3:0] out_0, out_1, out_2, out_3, out_4;
   wire [31:0] data_A, data_B;
   wire        clk, reset_L;
   wire [14:0] sel_A, sel_B;
   wire [4:0]  sel;
   wire [3:0]  menor,mayor,menor_synth,mayor_synth;


   top top__ (/*AUTOINST*/
	      // Outputs
	      .mayor			(mayor[3:0]),
	      .menor			(menor[3:0]),
	      // Inputs
	      .clk			(clk),
	      .reset_L			(reset_L),
	      .data_A			(data_A[31:0]),
	      .data_B			(data_B[31:0]),
	      .sel_A			(sel_A[14:0]),
	      .sel_B			(sel_B[14:0]),
	      .sel			(sel[4:0]));

    top_synth synth (/*AUTOINST*/
         // Outputs
         .mayor_synth			(mayor_synth[3:0]),
         .menor_synth			(menor_synth[3:0]),
         // Inputs
         .clk			(clk),
         .reset_L			(reset_L),
         .data_A			(data_A[31:0]),
         .data_B			(data_B[31:0]),
         .sel_A			(sel_A[14:0]),
         .sel_B			(sel_B[14:0]),
         .sel			(sel[4:0]));



   probador probador(/*AUTOINST*/
		     // Outputs
		     .clk		(clk),
		     .reset_L		(reset_L),
		     .data_A		(data_A[31:0]),
		     .data_B		(data_B[31:0]),
		     .sel_A		(sel_A[14:0]),
		     .sel_B		(sel_B[14:0]),
		     .sel		(sel[4:0]),
		     // Inputs
         .mayor			(mayor[3:0]),
         .menor			(menor[3:0]),
         .mayor_synth			(mayor_synth[3:0]),
         .menor_synth			(menor_synth[3:0]));





endmodule
