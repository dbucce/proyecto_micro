module nibble_mayor(
		    input 	     clk,
		    input 	     reset_L,
		    input [3:0]      nibble_0,
		    input [3:0]      nibble_1,
		    input [3:0]      nibble_2,
		    input [3:0]      nibble_3,
		    input [3:0]      nibble_4,
		    output reg [3:0] mayor);

   reg [3:0] 			     temp_mayor;
   reg [3:0] 			     nibbles [4:0];


   integer 			     i;

   always @(*)begin
      temp_mayor = nibbles[0];

      for(i=0;i<=4;i=i+1)begin
	 if(nibbles[i]>temp_mayor)begin
	    temp_mayor = nibbles[i];
	 end
      end
   end


   always @(posedge clk)begin
      if(!reset_L)begin
	 mayor <= 'b0000;
	 nibbles[0] <= 0;
	 nibbles[1] <= 0;
	 nibbles[2] <= 0;
	 nibbles[3] <= 0;
	 nibbles[4] <= 0;

      end
      else begin
	 mayor <= temp_mayor;
	 nibbles[0] <= nibble_0;
	 nibbles[1] <= nibble_1;
	 nibbles[2] <= nibble_2;
	 nibbles[3] <= nibble_3;
	 nibbles[4] <= nibble_4;
      end // else: !if(!reset_L)
   end // always @ (posedge clk)


endmodule
