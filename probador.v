module probador (
		output reg clk,
		output reg reset_L,
		output reg [31:0] data_A,
		output reg [31:0] data_B,
		output reg [14:0] sel_A,
		output reg [14:0] sel_B,
		output reg [4:0] sel,
		input [3:0] mayor,
	  input [3:0] menor,
	  input [3:0] mayor_synth,
	  input [3:0] menor_synth);

initial begin
	$dumpfile("selector.vcd");
	$dumpvars;
	{clk, reset_L, data_A, data_B, sel_A, sel_B, sel} = 0;

  @ (posedge clk)
  	reset_L <= 1'b0;
  @ (posedge clk)
		reset_L <= 1'b1;
  @ (posedge clk)
	 	data_A <= $random;
		data_B <= $random;
		sel_A <= $random;
		sel_B <= $random;
		sel <= 'b1010;
  @ (posedge clk)

  @ (posedge clk)

	@ (posedge clk)

	 #10 $finish;
end

initial clk <=1; //Se crea la senal de reloj
always #2 clk <= ~clk;

endmodule
