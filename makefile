all: yosys verilog gtkwave

yosys:
	yosys -s top.tcl -l logfile.log
	sed -i 's/top/top_synth/g' top_synth.v
	sed -i 's/mayor/mayor_synth/g' top_synth.v
	sed -i 's/menor/menor_synth/g' top_synth.v
	sed -i 's/selector5/sel5_synth/g' top_synth.v
	sed -i 's/selector/sel_synth/g' top_synth.v
	sed -i 's/nibble_menor/nibble_menor_synth/g' top_synth.v
	sed -i 's/nibble_mayor/nibble_mayor_synth/g' top_synth.v

verilog:
	iverilog -o p.out bancoprueba.v
	vvp p.out

gtkwave:
	gtkwave top.gtkw

clean:
	@rm -rf *~
	@rm -rf *#
	@rm -rf *.out
	@rm -rf *.vcd
