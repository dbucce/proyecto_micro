`include "selector5.v"
`include "nibble_mayor.v"
`include "nibble_menor.v"

module top(	   
		   input 	clk,
		   input 	reset_L,
		   input [31:0] data_A,
		   input [31:0] data_B,
		   input [14:0] sel_A,
		   input [14:0] sel_B,
		   input [4:0] 	sel,
		   output [3:0] mayor,
		   output [3:0] menor);

   
   wire [3:0] 			out_0, out_1, out_2, out_3, out_4;

   /*AUTOWIRE*/
    selector5 selector(/*AUTOINST*/
		       // Outputs
		       .out_0		(out_0[3:0]),
		       .out_1		(out_1[3:0]),
		       .out_2		(out_2[3:0]),
		       .out_3		(out_3[3:0]),
		       .out_4		(out_4[3:0]),
		       // Inputs
		       .clk		(clk),
		       .reset_L		(reset_L),
		       .data_A		(data_A[31:0]),
		       .data_B		(data_B[31:0]),
		       .sel_A		(sel_A[14:0]),
		       .sel_B		(sel_B[14:0]),
		       .sel		(sel[4:0]));

   nibble_mayor mayor__(
		      // Outputs
		      .mayor		(mayor[3:0]),
		      // Inputs
		      .clk		(clk),
		      .reset_L		(reset_L),
		      .nibble_0		(out_0[3:0]),
		      .nibble_1		(out_1[3:0]),
		      .nibble_2		(out_2[3:0]),
		      .nibble_3		(out_3[3:0]),
		      .nibble_4		(out_4[3:0]));

   nibble_menor menor__(
		      // Outputs
		      .menor		(menor[3:0]),
		      // Inputs
		      .clk		(clk),
		      .reset_L		(reset_L),
		      .nibble_0		(out_0[3:0]),
		      .nibble_1		(out_1[3:0]),
		      .nibble_2		(out_2[3:0]),
		      .nibble_3		(out_3[3:0]),
		      .nibble_4		(out_4[3:0]));
   
   

endmodule // top
