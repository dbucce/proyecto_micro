`include "selector.v"
module selector5(
	input clk,
	input reset_L,
	input [31:0] data_A,
	input [31:0] data_B,
	input [14:0] sel_A,
	input [14:0] sel_B,
	input [4:0] sel,
	output [3:0] out_0,
  output [3:0] out_1,
  output [3:0] out_2,
  output [3:0] out_3,
  output [3:0] out_4);

  selector sel0(
        .clk		(clk),
        .reset_L		(reset_L),
        .data_A    (data_A),
        .data_B    (data_B),
        .selector_A   (sel_A[2:0]),
        .selector_B   (sel_B[2:0]),
        .selector     (sel[0]),
        .out   (out_0)
        );
  selector sel1(
        .clk		(clk),
        .reset_L		(reset_L),
        .data_A    (data_A),
        .data_B    (data_B),
        .selector_A   (sel_A[5:3]),
        .selector_B   (sel_B[5:3]),
        .selector     (sel[1]),
        .out   (out_1)
        );
  selector sel2(
        .clk		(clk),
        .reset_L		(reset_L),
        .data_A    (data_A),
        .data_B    (data_B),
        .selector_A   (sel_A[8:6]),
        .selector_B   (sel_B[8:6]),
        .selector     (sel[2]),
        .out   (out_2)
        );
  selector sel3(
        .clk		(clk),
        .reset_L		(reset_L),
        .data_A    (data_A),
        .data_B    (data_B),
        .selector_A   (sel_A[11:9]),
        .selector_B   (sel_B[11:9]),
        .selector     (sel[3]),
        .out   (out_3)
        );
  selector sel4(
         .clk		(clk),
         .reset_L		(reset_L),
         .data_A    (data_A),
         .data_B    (data_B),
         .selector_A   (sel_A[14:12]),
         .selector_B   (sel_B[14:12]),
         .selector     (sel[4]),
         .out   (out_4)
         );

endmodule
