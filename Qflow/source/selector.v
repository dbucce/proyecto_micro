module selector(
	input clk,
	input reset_L,
	input [31:0] data_A,
	input [31:0] data_B,
	input [2:0] selector_A,
	input [2:0] selector_B,
	input selector,
	output reg[3:0] out);

	always @(posedge clk) begin
		if (!reset_L) begin
			out <= 'h0;
		end
		else if (!selector) begin // selecciona A
			if (selector_A == 0)begin
				out <= data_A[3:0];
			end
			else if (selector_A == 1)begin
				out <= data_A[7:4];
			end
			else if (selector_A == 2)begin
				out <= data_A[11:8];
			end
			else if (selector_A == 3)begin
				out <= data_A[15:12];
			end
			else if (selector_A == 4)begin
				out <= data_A[19:16];
			end
			else if (selector_A == 5)begin
				out <= data_A[23:20];
			end
			else if (selector_A == 6)begin
				out <= data_A[27:24];
			end
			else if (selector_A == 7)begin
				out <= data_A[31:28];
			end
		end
		else begin // selecciona B
			if (selector_B == 0)begin
				out <= data_B[3:0];
			end
			else if (selector_B == 1)begin
				out <= data_B[7:4];
			end
			else if (selector_B == 2)begin
				out <= data_B[11:8];
			end
			else if (selector_B == 3)begin
				out <= data_B[15:12];
			end
			else if (selector_B == 4)begin
				out <= data_B[19:16];
			end
			else if (selector_B == 5)begin
				out <= data_B[23:20];
			end
			else if (selector_B == 6)begin
				out <= data_B[27:24];
			end
			else if (selector_B == 7)begin
				out <= data_B[31:28];
			end
		end
  end
endmodule
