#!/bin/tcsh -f
#-------------------------------------------
# qflow variables for project /home/alberto/Documents/Microelectronica/Proyecto/GIT/proyecto_micro/Qflow
#-------------------------------------------

set projectpath=/home/alberto/Documents/Microelectronica/Proyecto/GIT/proyecto_micro/Qflow
set techdir=/usr/local/share/qflow/tech/osu018
set sourcedir=/home/alberto/Documents/Microelectronica/Proyecto/GIT/proyecto_micro/Qflow/source
set synthdir=/home/alberto/Documents/Microelectronica/Proyecto/GIT/proyecto_micro/Qflow/synthesis
set layoutdir=/home/alberto/Documents/Microelectronica/Proyecto/GIT/proyecto_micro/Qflow/layout
set techname=osu018
set scriptdir=/usr/local/share/qflow/scripts
set bindir=/usr/local/share/qflow/bin
set logdir=/home/alberto/Documents/Microelectronica/Proyecto/GIT/proyecto_micro/Qflow/log
#-------------------------------------------

