read_verilog -noopt top.v 
hierarchy -check -top top
proc; opt; fsm; opt; memory; opt
techmap; opt
flatten
dfflibmap -liberty osu018_stdcells.lib
abc -D 250 -constr timing.constr -liberty osu018_stdcells.lib
stat -liberty osu018_stdcells.lib
opt_clean -purge
write_verilog top_synth.v
